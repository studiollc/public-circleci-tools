#!/usr/bin/env bash
set -euo pipefail

CIRCLECI=${CIRCLECI:-false}
CIRCLECI_MACOS=${CIRCLECI_MACOS:-false}

if [[ $CIRCLECI != true ]]; then
  echo "This should only be run on circleci as it will replace the current user's ssh keys! Exiting."
  exit 1;
fi

# exit;

echo "Setting up SSH..."
mkdir -p ~/.ssh

# Add github and bitbucket's pubkeys
cat > ~/.ssh/known_hosts <<-EOF
  github.com ssh-rsa AAAAB3NzaC1yc2EAAAABIwAAAQEAq2A7hRGmdnm9tUDbO9IDSwBK6TbQa+PXYPCPy6rbTrTtw7PHkccKrpp0yVhp5HdEIcKr6pLlVDBfOLX9QUsyCOV0wzfjIJNlGEYsdlLJizHhbn2mUjvSAHQqZETYP81eFzLQNnPHt4EVVUh7VfDESU84KezmD5QlWpXLmvU31/yMf+Se8xhHTvKSCZIFImWwoG6mbUoWf9nzpIoaSjB+weqqUUmpaaasXVal72J+UX2B+2RPW3RcT0eOzQgqlJL3RKrTJvdsjE3JEAvGq3lGHSZXy28G3skua2SmVi/w4yCE6gbODqnTWlg7+wC604ydGXA8VJiS5ap43JXiUFFAaQ==
  bitbucket.org ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDQeJzhupRu0u0cdegZIa8e86EG2qOCsIsD1Xw0xSeiPDlCr7kq97NLmMbpKTX6Esc30NuoqEEHCuc7yWtwp8dI76EEEB1VqY9QJq6vk+aySyboD5QF61I/1WeTwu+deCbgKMGbUijeXhtfbxSxm6JwGrXrhBdofTsbKRUsrN1WoNgUa8uqN1Vx6WAJw1JHPhglEGGHea6QICwJOAr/6mrui/oB7pkaWKHj3z7d1IC4KWLtY47elvjbaTlkN04Kc/5LFEirorGYVbt15kAUlqGM65pk6ZBxtaO3+30LVlORZkxOh+LKL/BvbZ/iRNhItLqNyieoQj/uh/7Iv4uyH/cV/0b4WDSd3DptigWq84lJubb9t/DnZlrJazxyDCulTmKdOR7vs9gMTo+uoIrPSb8ScTtvw65+odKAlBj59dhnVp9zd7QUojOpXlL62Aw56U4oO+FALuevvMjiWeavKhJqlR7i5n9srYcrNV7ttmDw7kf/97P5zauIhxcjX+xHv4M=
EOF

# add our public key
cat > ~/.ssh/id_rsa.pub <<-EOF
  ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDMfTMWTXG+zRxVQ3oEYBomZ6apZaujusZ6cIr6bKTN1mU70g0cupM2hOtg7LuKvNVmDcXvgSoQl77O09i8Ba271KHenF6q/rXG8S+cmt1L8MJHN5qSUBRJXp289XoaBqCXItu8iRfcxg7dwz4R86Q9uMGX4Qbm7n3e/fgWNq1fs3tWz45WCcyMAOrHOo0OjwE2POqSGHkDP+EWsJWSE1yR47yTW/8X5mNTskpM4D8mTMuLJWuEcnHuEKi2p7s36LtK/D+xL8dg6gA47MqnAGo5KiMx3D7E7u/NT/qd/8U9BkhCigGSFjBf/+B/Wznew3GNr0g8W8Sp78gNqLrtH6Gp circleci
EOF

# Extract the base64-encoded private key from environment variable $GIT_SSH_KEY
if [[ $CIRCLECI_MACOS == true ]]; then
  echo ${GIT_SSH_KEY} | tr "" "\n" | base64 -D > ~/.ssh/id_rsa
else
  echo ${GIT_SSH_KEY} | tr " " "\n" | base64 -d > ~/.ssh/id_rsa
fi

# Set proper file permissions
if [[ $CIRCLECI_MACOS == true ]]; then
  sudo chmod 700 ~
  sudo chmod 700 ~/.ssh
  sudo chmod 600 ~/.ssh/*
else
  chmod 700 ~
  chmod 700 ~/.ssh
  chmod 600 ~/.ssh/*
fi

# add key to ssh-agent
ssh-add

# git configuration
git config --global user.email "jenkins@whoisstudio.com"
git config --global user.name "Leroy Jenkins"
