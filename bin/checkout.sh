#!/usr/bin/env bash
set -eo pipefail

echo "Checking out code..."

# Workaround old docker images with incorrect $HOME
# check https://github.com/docker/docker/issues/2968 for details
if [ "${HOME}" = "/" ]
then
  export HOME=$(getent passwd $(id -un) | cut -d: -f6)
fi

# force use of git protocol
git config --global url."git@bitbucket.org".insteadOf "https://bitbucket.org" || true

# create the working directory
WORKDIR="${CIRCLE_WORKING_DIRECTORY/#\~/$HOME}"
mkdir -p $WORKDIR
cd $WORKDIR

# clone the repository
echo "Cloning" $CIRCLE_REPOSITORY_URL
git clone "${CIRCLE_REPOSITORY_URL}" .

# checkout the appropriate tag/branch/commit
if [ -n "$CIRCLE_TAG" ]
then
  git reset --hard "$CIRCLE_SHA1"
  git checkout -q "$CIRCLE_TAG"
elif [ -n "$CIRCLE_BRANCH" ]
then
  git reset --hard "$CIRCLE_SHA1"
  git checkout -q -B "$CIRCLE_BRANCH"
fi

git reset --hard "$CIRCLE_SHA1"
