#!/usr/bin/env bash
set -euo pipefail

tr " " "\n" | base64 -d
