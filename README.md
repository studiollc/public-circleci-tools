# public-circleci-tools

> This is a PUBLIC repo! Do __NOT__ put anything sensitive in here!

Various tools for `circleci`

- `bin/setup-ssh.sh` – Sets up `ssh` on the machine so it can access our private repositories on Bitbucket.

- `bin/checkout.sh` – Clones the git repository and checks out appropriate code based on `circleci` environment variables.

- `bin/encode_key.sh` – Encodes a key for storage in an environment variable.

- `bin/decode_key.sh` – Decodes a previously-encoded key back to its original form.
